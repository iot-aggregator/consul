#!/bin/sh

sed -i "s|MINIO_ADDRESS|${MINIO_ADDRESS}|g" /consul/config/minio.json
sed -i "s|MINIO_PORT|${MINIO_PORT}|g" /consul/config/minio.json

sed -i "s|PROMETHEUS_ADDRESS|${PROMETHEUS_ADDRESS}|g" /consul/config/prometheus.json
sed -i "s|PROMETHEUS_PORT|${PROMETHEUS_PORT}|g" /consul/config/prometheus.json

sed -i "s|RABBIT_ADDRESS|${RABBIT_ADDRESS}|g" /consul/config/rabbit.json
sed -i "s|RABBIT_PORT|${RABBIT_PORT}|g" /consul/config/rabbit.json
sed -i "s|RABBIT_MANAGEMENT_PORT|${RABBIT_MANAGEMENT_PORT}|g" /consul/config/rabbit.json

sed -i "s|GRAFANA_ADDRESS|${GRAFANA_ADDRESS}|g" /consul/config/grafana.json
sed -i "s|GRAFANA_PORT|${GRAFANA_PORT}|g" /consul/config/grafana.json

consul agent -server -data-dir /consul/data -config-dir /consul/config -ui -node=server-1 -bootstrap-expect=1 -client=0.0.0.0

