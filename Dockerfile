FROM consul

COPY ./config /consul/config

COPY ./custom-entrypoint.sh /custom-entrypoint.sh

RUN ["chmod", "+x", "/custom-entrypoint.sh"]

CMD ["sh", "/custom-entrypoint.sh"]